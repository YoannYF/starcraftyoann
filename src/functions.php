<?php 


function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return Markdown::defaultTransform($string_markdown_formatted);
}

function getTerrans(){

    $terran = Requests::get('https://allosaurus.delahayeyourself.info/api/starcraft/terran/units/');

    if($terran->status_code == 200){
        return json_decode($terran->body);
    }

    return array();

}

function getZergs(){
    
    $zerg = Requests::get('https://allosaurus.delahayeyourself.info/api/starcraft/zerg/units/');

    if($zerg->status_code == 200){
        return json_decode($zerg->body);
    }

    return array();
}

function getUnitTerrans($slug){

    $unit_terrans = sprintf('https://allosaurus.delahayeyourself.info/api/starcraft/terran/units/%s', $slug);

    $response = Requests::get($unit_terrans);

    if($response->status_code == 200){
        return json_decode($response->body);
    }

    return null;
}

function getUnitZergs($slug){

    $unit_zergs = sprintf('https://allosaurus.delahayeyourself.info/api/starcraft/zerg/units/%s', $slug);

    $response = Requests::get($unit_zergs);
    
    if($response->status_code == 200){
        return json_decode($response->body);
    }

    return null;
}