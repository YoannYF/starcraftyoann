<?php
/// DEBUT Initialisation ///

require "vendor/autoload.php";
use Michelf\Markdown;

$loader = new \Twig\Loader\FilesystemLoader(dirname(__FILE__) . '/templates');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', '\Twig\Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new \Twig\Extension\DebugExtension());

    $twig->addFilter(new \Twig\TwigFilter('markdown', function($string){
        return Markdown::defaultTransform($string);
    }));

});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

/// FIN Initialisation ///

/// DEBUT Site ///

Flight::route('/', function(){

    Flight::render('index.twig');
});

Flight::route('/espece/@name_race', function($name_race){

    if($name_race == "terran"){
        $data = array(
            'terrans' => getTerrans(),
            'race' => $name_race,
        );
    }elseif($name_race == "zerg"){
        $data = array(
            'zergs' => getZergs(),
            'race' => $name_race,
        );
    }

    Flight::render('espece.twig', $data);
});

Flight::route('/espece/@name_race/@name_unit', function($name_race, $name_unit){

    if($name_race == "terran"){
        $data = array(
            'personnages_terran' => getUnitTerrans($name_unit),
            'race' => $name_race,
        );
    }elseif($name_race == "zerg"){
        $data = array(
            'personnages_zerg' => getUnitZergs($name_unit),
            'race' => $name_race,
        );
    }

    Flight::render('unit.twig', $data);
});

Flight::start();